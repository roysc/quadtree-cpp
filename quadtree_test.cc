#include <utility>
#include <random>
#include <chrono>
#include <memory>

#include "util/io.hh"

#include "quadtree.hh"

template <class F>
std::chrono::duration<double, std::milli> benchmark(F&& f)
{
    using Clock = std::chrono::high_resolution_clock;
    auto start = Clock::now();
    f();
    auto end = Clock::now();
    return end - start;
}


int main(int argc, const char** argv)
{
    using quadtree::Quadtree;
    using quadtree::Point;

    int n = 1000;
    if (argc >= 2) {
        n = std::stoi(argv[1]);
        if (n < 0) {
            util::println("enter a positive integer");
            return 1;
        }
    }

    Quadtree<int> tree({0, 0}, 1, 1);

    std::uniform_real_distribution<double> dist_pt {-.5, .5};
    std::uniform_int_distribution<int> dist_val;
    std::random_device rd;
    std::mt19937 g{rd()};

    std::vector<std::pair<Point, int> > points;
    for (int i = 0; i < n; ++i) {
        Point pt = {dist_pt(g), dist_pt(g)};
        points.emplace_back(pt, dist_val(g));
    }

    auto t_insert = benchmark([&] { for (auto&& e: points) { tree.insert(e); } });

    util::println("size = ", tree.size());
    util::println("nodes = ", tree.node_count());
    util::println("time = ", t_insert.count(), " ms");
}
