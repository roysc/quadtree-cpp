#!/bin/bash

WARN="-Werror=return-type -Wunreachable-code"
CMD="clang++ --std=c++1z -O $WARN quadtree_test.cc -o quadtree"

echo "$CMD"
$CMD
