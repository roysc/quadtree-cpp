#include <memory>
#include <utility>
#include <boost/variant.hpp>
#include <boost/range/numeric.hpp>

// #include "util/io.hh"

namespace quadtree
{

using std::unique_ptr;
using std::pair;
using std::move;
using boost::get;

using Point = std::array<double, 2>;

const unsigned max_bucket_capacity = 16;
const unsigned max_node_depth = 32;

template <class T>
struct Node
{
    using Value = pair<Point, T>;
    struct Branch { std::array<unique_ptr<Node>, 4> children; };
    struct Bucket { std::vector<Value> data; };
    using Variant = boost::variant<Branch, Bucket>;
    
    const Point _center;
    Variant _variant = Bucket{};

    Node(Point c, Bucket v): _center(c), _variant(move(v)) {}
    Node(Point c): Node(c, Bucket{}) {}
    
    template <class F>
    void traverse(F&& f) const
    {
        f(*this);
        if (auto branch = get<Branch>(&_variant)) {
            for (auto&& child: branch->children) {
                child->traverse(f);
            }
        }
    }

    unsigned quadrant(Point p) const
    {
        bool x_less = p[0] < _center[0], y_less = p[1] < _center[1];
        if (x_less && y_less) {
            return 0;
        } else if (y_less) {
            return 1;
        } else if (x_less) {
            return 2;
        } else {
            return 3;
        }
    }
    
    // ret => (node inserted)
    pair<Node*, unsigned> insert(unsigned depth, Value value)
    {
        // util::println("push(depth=", depth, ", pt=", get<0>(value), ", _) (Node (center=", _center, "))");
        if (auto branch = get<Branch>(&_variant)) {
            auto q = quadrant(get<0>(value));
            return branch->children[q]->insert(depth - 1, move(value));
        } else {
            auto bucket = get<Bucket>(&_variant);
            bucket->data.push_back(move(value));
            if (bucket->data.size() == max_bucket_capacity && depth != 0) {
                return {this, depth};
            }
            return {};
        }
    }

    void split(double dx, double dy)
    {
        // util::println("split(dx = ", dx, ", dy = ", dy, ") center=", _center);
        if (auto bucket = get<Bucket>(&_variant)) {

            auto make_center = [
                origin = _center, dx, dy
            ] (unsigned quadrant) -> Point {
                double x = origin[0], y = origin[1];
                if (quadrant % 2 == 0) {
                    x -= dx;
                } else {
                    x += dx;
                }
                if (quadrant < 2) {
                    y -= dy;
                } else {
                    y += dy;
                };
                return {x, y};
            };

            std::vector<Bucket> child_data(4);
            for (auto& e: bucket->data) {
                child_data[quadrant(get<0>(e))].data.push_back(move(e));
            }

            Branch branch;
            unsigned i{};
            for (auto& e: branch.children) {
                e.reset(new Node {make_center(i++), move(child_data.back())});
                child_data.pop_back();
            }

            _variant = move(branch);
        }
    }
};

template <class T>
struct Quadtree
{
    using Value = pair<const Point, T>;

    Node<T> _root;
    double _width, _height;
    
    Quadtree(Point center, double width, double height):
        _root{center}, _width{width}, _height{height}
    {}

    size_t size() const
    {
        size_t sum{};
        _root.traverse([&] (auto&& c) {
                if (auto bucket = get<typename Node<T>::Bucket>(&c._variant))
                    sum += bucket->data.size();
            });
        return sum;
    }

    size_t node_count() const
    {
        size_t sum{};
        _root.traverse([&] (auto&&) { sum += 1; });
        return sum;
    }

    void insert(Value value)
    {
        auto result = _root.insert(max_node_depth, move(value));
        if (auto node = get<0>(result)) {
            // divide w, h by 2^(max_node_depth - depth + 1)
            unsigned depth = max_node_depth - get<1>(result) + 1;
            double denom = static_cast<double>(2 << depth);
            node->split(_width / denom, _height / denom);
        }
    }

    template <class... Args>
    void emplace(Args&&... args)
    {
        insert(Value{std::forward<Args>(args)...});
    }
};


}
